// Setup express dependency
const express = require("express");
// Create a Router instance
const router = express.Router();
// import TaskController.js
const taskController = require("../controllers/taskController");

// Routes

// Route to get all the tasks
router.get("/", (req, res) => {
    taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

router.post("/", (req, res) => {
    taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for deleting a task
// localhost:3001/tasks/
router.delete("/:id", (req, res) => {
    taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

// Route for updating a task
router.put("/:id", (req, res) => {
    taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// ACTIVITY
router.get("/:id", (req, res) => {
    taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

router.put("/:id/complete", (req, res) => {
    taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;

